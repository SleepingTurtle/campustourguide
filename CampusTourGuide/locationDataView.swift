//
//  locationDataView.swift
//  CampusTourGuide
//
//  Created by Joshua Charles on 6/7/15.
//  Copyright (c) 2015 Joshua Charles. All rights reserved.
//

import UIKit

class locationDataView: UITableView {
    
    var locations: [Locations] = locationData
    
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationData.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("locData", forIndexPath: indexPath)
            as! UITableViewCell
        
        let locationData = locations[indexPath.row] as Locations
        cell.textLabel?.text = locationData.Name
        return cell
    }


}


