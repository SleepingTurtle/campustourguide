//
//  SampleData.swift
//  CampusTourGuide
//
//  Created by Joshua Charles on 6/7/15.
//  Copyright (c) 2015 Joshua Charles. All rights reserved.
//

let locationData = [Locations(Name: "JON M. HUNTSMAN HALL"),
    Locations(Name: "KELLY WRITER’S HOUSE"),
    Locations(Name: "CIVIC HOUSE"),
    Locations(Name: "LGBT CENTER"),
    Locations(Name: "HIPCITYVEG"),
    Locations(Name: "DISTRITO"),
    Locations(Name: "ABNER’S"),
    Locations(Name: "GREENFIELD INTERCULTURAL CENTER"),
    Locations(Name: "POTTRUCK HEALTH AND FITNESS CENTER")]